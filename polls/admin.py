from django.contrib import admin

# Register your models here.
from polls.models import Servicio, Independiente

admin.site.register(Servicio)
admin.site.register(Independiente)