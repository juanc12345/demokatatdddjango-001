from time import sleep

from selenium.webdriver.common.by import By
from unittest import TestCase
from selenium import webdriver


class Functionaltest(TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def test_title(self):
        self.browser.get('http://127.0.0.1:8000/')
        self.assertIn('Busco Ayuda', self.browser.title)


    def test_registro(self):
        self.browser.get('http://127.0.0.1:8000/')
        link = self.browser.find_element_by_id('id_registro')
        link.click()

        nombre = self.browser.find_element_by_id('id_nombres')
        nombre.send_keys('Pedro')

        apellidos = self.browser.find_element_by_id('id_apellidos')
        apellidos.send_keys('Casas')

        correo = self.browser.find_element_by_id('id_correo')
        correo.send_keys('pedro@email.com')

        experiencia = self.browser.find_element_by_id('id_experiencia')
        experiencia.send_keys('10')

        self.browser.find_element_by_xpath("//select[@id='id_servicio']/option[text()='Ingeniero']").click()

        telefono = self.browser.find_element_by_id('id_telefono')
        telefono.send_keys('3006785432')

        imagen = self.browser.find_element_by_id('id_foto')
        imagen.send_keys("d:\\temp\\img.jpg")

        password = self.browser.find_element_by_id('id_password')
        password.send_keys('pedro123')

        password = self.browser.find_element_by_id('id_password_confirm')
        password.send_keys('pedro123')

        botonGrabar = self.browser.find_element_by_id('id_grabar')
        botonGrabar.click()

        self.browser.implicitly_wait(3)
        span = self.browser.find_element(By.XPATH, '//span[text()="Pedro Casas"]')
        self.assertIn('Pedro Casas', span.text)

    def test_verdetalle(self):
        self.browser.get('http://127.0.0.1:8000/')
        span = self.browser.find_element(By.XPATH, '//div/span[text()="Pedro Casas"]/../img')
        span.click()
        sleep(0.5)

        h3 = self.browser.find_element(By.XPATH, '//h3[text()="Pedro Casas"]')
        self.assertIn('Pedro Casas', h3.text)